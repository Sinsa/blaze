package net.sinsa92.blaze.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/helloworld")
public class HelloWorldController {

    @GetMapping
    public String greet(@RequestParam(value = "name",required = false) String name) {
        if(name.isEmpty()) {
            return "Hello World!";
        }else{
            return "Hello World and " + name + "!";
        }
    }
}
