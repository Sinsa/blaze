package net.sinsa92.blaze.bot.process.bot.commands;

import net.sinsa92.blaze.bot.entities.Command;
import net.sinsa92.blaze.bot.entities.Usergroup;
import org.javacord.api.DiscordApi;
import org.javacord.api.event.message.MessageCreateEvent;

/**
 * The Ping command is used to measure the latency of Minako.
 *
 * @author Sinsa#2674
 * @see Command
 * @since 1.0.0
 */
public class Ping extends Command {

    public Ping(Usergroup minAccess, boolean serverOnly) {
        super(minAccess, serverOnly, "ping");
    }

    @Override
    public void execute(MessageCreateEvent event, DiscordApi api, String[] params) {
        event.getChannel().sendMessage("My ping is " + api.getLatestGatewayLatency().toMillis() + "ms.");
    }

}
