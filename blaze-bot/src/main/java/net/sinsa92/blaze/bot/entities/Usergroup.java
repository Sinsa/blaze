package net.sinsa92.blaze.bot.entities;

public enum Usergroup {
    HOST,
    MAPPER_MAPPOOLER,
    STREAMER_COMMENTATOR,
    REFEREE,
    PLAYER,
    USER;

    /**
     * This Method resolves a Usergroup to an integer so they are comparable.
     *
     * @param usergroup An Enum of Usergroup Type.
     * @return an Integer from 0-5.
     */
    public static int getValueOfUsergroup(Usergroup usergroup) {
        switch (usergroup) {
            case HOST:
                return 5;
            case MAPPER_MAPPOOLER:
                return 4;
            case STREAMER_COMMENTATOR:
                return 3;
            case REFEREE:
                return 2;
            case PLAYER:
                return 1;
            default:
                return 0;
        }
    }
}
