package net.sinsa92.blaze.bot.process.bot;

import net.sinsa92.blaze.bot.entities.Usergroup;
import net.sinsa92.blaze.bot.process.bot.commands.Ping;
import net.sinsa92.blaze.bot.process.utils.SpringBeanUtils;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class Bot {

    static Logger messageLogger = LoggerFactory.getLogger("net.sinsa92.blaze.bot.process.bot.Bot.MessageLogger");
    static Logger logger = LoggerFactory.getLogger(Bot.class);
    Ping ping = new Ping(Usergroup.USER, false);
    DiscordApi discordApi;

    public void init(String[] args) {
        discordApi = new DiscordApiBuilder().setToken(getProperty("discord.bot.token")).setAllIntents().login().join();
        logger.info("<init> I am ready!");

        discordApi.addMessageCreateListener(event -> {
            messageLogger.trace("{}: {}",event.getMessageAuthor().getDiscriminatedName(), event.getMessageContent());

            if(event.getMessageAuthor().isBotUser()) {
                return;
            }
            if(!event.getMessageContent().startsWith(getProperty("discord.bot.prefix"))) {
                return;
            }

            String[] params = event.getMessageContent().substring(getProperty("discord.bot.prefix").length()).split("\\s+");
            String baseCommand = params[0].toLowerCase();
            params = Arrays.stream(params).skip(1).toArray(String[]::new);
            try {
                switch (baseCommand){
                    case "hi":
                        logger.debug("hi command was triggered");
                        event.getChannel().sendMessage("Hello World!");
                        break;
                    case "ping":
                        ping.execute(event, discordApi, params);
                }
            } catch (Exception e) {
                logger.error(e.toString());
                e.printStackTrace();
            }
        });
    }

    private String getProperty(String property) {
        return SpringBeanUtils.getBean(Environment.class).getProperty(property);
    }
}
