package net.sinsa92.blaze.bot.entities;

import org.javacord.api.DiscordApi;
import org.javacord.api.event.message.MessageCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Command {
    static Logger logger = LoggerFactory.getLogger(Command.class);
    protected Usergroup minAccess;
    protected String commandName;
    protected boolean serverOnly;

    public Command(Usergroup minAccess, boolean serverOnly, String commandName) {
        this.minAccess = minAccess;
        this.serverOnly = serverOnly;
        this.commandName = commandName;
    }

    public boolean isServerOnly() {
        return serverOnly;
    }

    public Usergroup getMinAccess() {
        return minAccess;
    }

    public String getCommandName() {
        return commandName;
    }

    public void execute(MessageCreateEvent event, DiscordApi api, String[] params) {
        logger.error("Overriding could not be successful! Command {} by {} was not executed!", this.getCommandName(), event.getMessageAuthor().getDiscriminatedName());
    }

}
