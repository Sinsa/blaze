package net.sinsa92.blaze.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlazeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlazeApplication.class, args);
//        Bot bot = new Bot();
//        bot.init(args);
    }
}
